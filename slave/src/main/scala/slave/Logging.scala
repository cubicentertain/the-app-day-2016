package slave

import com.typesafe.scalalogging.Logger
import org.slf4j.LoggerFactory

//Convenience trait for logging
trait Logging {
  val logger = Logger(LoggerFactory.getLogger(getClass))
}
