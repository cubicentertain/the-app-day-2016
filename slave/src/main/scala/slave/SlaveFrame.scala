package slave

import java.awt.{Color, Component, FlowLayout}
import java.awt.event.{ActionEvent, ActionListener, WindowAdapter, WindowEvent}
import java.net.{Socket, SocketException}
import javax.swing._

import protocol.{ChatMessage, ProtocolMessage}

//Main window component
class SlaveFrame(chat: ChatConnection, socket: Socket) extends JFrame with Logging {
  setSize(320, 205)
  getContentPane().setLayout(new FlowLayout())

  val messages = List("Hello World!", "Ping", "Pong", "Affirmative", "Negative", "+2")
  val jbuttons = messages.map(new JButton(_))
  val defaultModel = new DefaultListModel[ChatMessage]()
  val chatHistory = new JList[ChatMessage](defaultModel)
  chatHistory.setLayoutOrientation(JList.VERTICAL)
  chatHistory.setFixedCellHeight(12)
  chatHistory.setCellRenderer(new PriorityListCellRenderer)
  val scrollPane = new JScrollPane(chatHistory)

  val chatUpdateThread = chat.redirectToTextArea(socket, defaultModel, scrollPane)
  chatUpdateThread.start()

  //Add button listeners
  jbuttons foreach { button =>
    button.addActionListener(
      new ActionListener() {
        def actionPerformed(e: ActionEvent) {
          if (jbuttons.contains(e.getSource)) {
            val content = button.getText
            val message = ChatMessage("slave", content)
            ProtocolMessage.writeToStream(socket.getOutputStream, message)
            logger.trace(s"Writing message: [$message]")
          }
        }
      })
    add(button)
  }

  //Shutdown application if GUI closed
  addWindowListener(new WindowAdapter() {
    override
    def windowClosing(windowEvent: WindowEvent) {
      try {
        socket.close()
      } catch {
        case e: SocketException =>
      }

      logger.debug("Socket connection closed!")
      System.exit(0)

    }
  })

  add(scrollPane)
  setVisible(true)
}

//Highlight master messages on slaves
class PriorityListCellRenderer extends ListCellRenderer[ChatMessage] {
  val delegateCellRenderer = new DefaultListCellRenderer().asInstanceOf[ListCellRenderer[ChatMessage]]
  override def getListCellRendererComponent(list: JList[_<:ChatMessage], value: ChatMessage, index: Int, isSelected: Boolean, cellHasFocus: Boolean): Component = {
    val c = delegateCellRenderer.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus).asInstanceOf[JLabel]
    value match {
      case ChatMessage(sender, content) => {
        c.setText(content)
        if (sender == "master") {
          c.setBackground(Color.GREEN)
        }
      }
    }
    c
  }
}