package slave

//Parser for command line options
object CommandLineParser {
  def parse(args: Array[String]): ConnectionParameters = {
    var host = "127.0.0.1"
    var port = 1234
    var ledPort = 0

    if (args.length < 4 || args(0) != "--host") {
      syntaxError()
    } else {
     if(args(0) == "--host"){
       host = args(1)
     } else{
       syntaxError()
     }
      if(args(2) == "--port"){
        port = Integer.parseInt(args(3))
      } else{
        syntaxError()
      }

     if (args.length >= 6) {
        if (args(4) == "--led") {
          ledPort = Integer.parseInt(args(5))
        }
       else{
          syntaxError()
        }
      }
    }

      ConnectionParameters(host, port, ledPort)
    }
  def syntaxError(): Unit ={
    println("Slave syntax:")
    println("--host <ip address> --port <tcp port> [--led <GPIO port>]")
    System.exit(1)
  }
}
