package slave

import java.io.{File, FileOutputStream, IOException, PrintStream}

//Entry point of every slave program
object Slave extends App with Logging{
  val clOptions = CommandLineParser.parse(args)
  logger.debug(s"Read CL Options: $clOptions")
  val led = clOptions.ledPort
  val chat = new ChatConnection(clOptions)
  val chatChannel = chat.connect()

  //Setup LED configuration
  try {
    new PrintStream(new FileOutputStream(new File(s"/sys/class/gpio/export"))).print(led)
    new PrintStream(new FileOutputStream(new File(s"/sys/class/gpio/gpio$led/direction"))).print("out")
  } catch {
    case e: IOException => logger.info(s"Could not find LED interface (port $led), failed with: ", e)
  }
  val gui = new SlaveFrame(chat,chatChannel)
}
