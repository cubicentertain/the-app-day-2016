package slave

import java.io._
import java.net.Socket
import java.text.SimpleDateFormat
import java.util.Date
import javax.swing.{DefaultListModel, JScrollPane, SwingUtilities}

import protocol.{ChatMessage, ProtocolMessage}

//Class to abstract from TCP stuff
class ChatConnection(connectionParameters: ConnectionParameters) extends Logging {
  val reader = new BufferedReader(new InputStreamReader(System.in))
  val dateFormat = new SimpleDateFormat("hh:mm:ss a")

  //Establish server connection to master
  def connect(): Socket = {
    logger.debug(s"Attempting to establish connection: $connectionParameters ....")
    val socket = new Socket(connectionParameters.host, connectionParameters.port)
    logger.debug("Connection established.")
    socket
  }

  //Write message string to master server
  def writeMessage(message: String, socket: Socket): Unit = {
    val out = new PrintWriter(socket.getOutputStream, true)

    out.print(s"$message\n")
    out.flush()
  }

  //Thread that permanently reads from the socket input stream and writes to GUI text area
  def redirectToTextArea(socket: Socket, listModel: DefaultListModel[ChatMessage], scrollPane: JScrollPane): Thread = {
    new Thread(new Runnable {
      def scrollToBottom(): Unit = {
        SwingUtilities.invokeLater(new Runnable {
          override def run(): Unit = {
            val scrollBar = scrollPane.getVerticalScrollBar
            scrollBar.setValue(scrollBar.getMaximum)
          }
        })
      }

      def run() {
        val in = socket.getInputStream

        try {
          while (true) {
            val message = ProtocolMessage.readFromStream(in)
            message match {
              case ChatMessage(sender, content) => {
                listModel.addElement(ChatMessage(sender,s"${dateFormat.format(new Date())}: $content"))
                scrollToBottom
                logger.debug(s"New message arrived: [$content]")
                println(content)
                blink()
              }
            }
          }
        } catch {
          case e: Exception =>
        }
      }
    })
  }

  //LED blinking
  def blink(): Unit = {
    try {
      for (x <- 1 to 4) {
        new PrintStream(new FileOutputStream(new File(s"/sys/class/gpio/gpio${connectionParameters.ledPort}/value"))).print(1)
        Thread.sleep(150)
        new PrintStream(new FileOutputStream(new File(s"/sys/class/gpio/gpio${connectionParameters.ledPort}/value"))).print(0)
        Thread.sleep(100)
      }
    } catch {
      case e: IOException => logger.info(s"Could not talk to LED interface, failed with: ", e)
    }
  }

}
