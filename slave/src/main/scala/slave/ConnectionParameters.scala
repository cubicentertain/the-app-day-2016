package slave

//Data container for command line options
case class ConnectionParameters(host:String, port: Int, ledPort: Int);
