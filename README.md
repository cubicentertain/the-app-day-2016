# Preface
In this project a light-weight Server/Client chat application has been realized.
The main design goal was to create a program tailored for the needs of modern credit-card sized single-board computers
like the Raspberry Pi for example by also maintaining platform independence.
In order to achieve this goal, the chat application has been written in Scala to run on the JVM. The chat protocol is based
on a basic JSON message format which can be extended easily by new message types in the future. At this moment two message
types are supported: master and slave messages.

# Project Structure
This project is realized as a Gradle build in order to build platform independently. In this project a master and slave
project can be build as stand-alone modules, representing the client and server side components of the chat program.

# Building
To build master/slave simply run `./gradlew uberjar` in the project root folder.
Generated executable jars can be found in the bin/ directory.

# Starting the Application
## Running the Server
`java -jar master-uberjar --host <host> --port <TCP port> [--led <GPIO port>]`

## Running the Slave
`java -jar slave-uberjar --port <TCP port> [--led <GPIO port>]`

## Troubleshooting
If the application is started on a remote host via ssh, the DISPLAY environment variable has to be set `:0`
(e.g., via `export DISPLAY=:0`) to display the chat GUI on host side.

# Frameworks
- Program code is written in [scala](http://www.scala-lang.org/).
- SLF4J is our logging framework: [slf4j](http://www.slf4j.org/).
- Build-tool is [gradle](https://gradle.org)
- [git](https://git-scm.com) and [gitlab](https://gitlab.org) were used for version control.
