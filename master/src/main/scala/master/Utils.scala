package master

import com.typesafe.scalalogging.Logger
import org.slf4j.LoggerFactory

object MagicConstants {
  val MessageBufferLength = 128
}

object Utils {
  def forever(f: => Unit): Unit = {
    while (true) {
      f
    }
  }
}

trait Logging {
  val logger = Logger(LoggerFactory.getLogger(getClass))
}