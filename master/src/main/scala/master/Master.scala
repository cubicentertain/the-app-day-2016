package master

import java.awt.event.{ActionEvent, ActionListener, WindowAdapter, WindowEvent}
import java.awt.{GridBagConstraints, GridBagLayout}
import java.io.IOException
import java.net.{ServerSocket, Socket}
import java.nio.charset.StandardCharsets
import java.nio.file.{Files, Paths}
import java.util.concurrent.ArrayBlockingQueue
import java.util.concurrent.atomic.AtomicInteger
import javax.swing._

import protocol.{ChatMessage, ProtocolMessage}

import scala.annotation.tailrec
import scala.collection.mutable
import scala.util.Try


trait Recipient {
  def writeMessage(message: ProtocolMessage)
}

class Slave(val id: Long, socket: Socket) extends Recipient {
  override def hashCode(): Int = id.hashCode()

  val out = socket.getOutputStream
  val in = socket.getInputStream

  override def equals(obj: scala.Any): Boolean = obj match {
    case other: Slave => id == other.id
    case _ => false
  }

  override def writeMessage(message: ProtocolMessage): Unit = {
    ProtocolMessage.writeToStream(out, message)
  }

  def readMessage(): ProtocolMessage = ProtocolMessage.readFromStream(in)

  override def toString: String = {
    socket.getRemoteSocketAddress.toString
  }
}

class MasterListener(masterSocket: ServerSocket, extraRecipients: Seq[Recipient] = Seq()) extends Runnable with Logging {
  var idCounter = 0L

  override def run(): Unit = {
    val broadcaster = new SlaveMessageBroadcaster
    extraRecipients.foreach(broadcaster.addRecipient)
    new Thread(broadcaster).start()
    val ui = new MasterUI(broadcaster)
    broadcaster.addRecipient(ui)
    ui.start()
    while (!masterSocket.isClosed) {
      val sock = masterSocket.accept()
      logger.info(s"Accepted connection from ${sock.getRemoteSocketAddress}")
      idCounter += 1
      val slave = new Slave(idCounter, sock)
      val producer = new SlaveMessageProducer(slave, broadcaster)
      new Thread(producer).start()
    }
  }
}

class SlaveMessageBroadcaster extends Runnable with Logging {
  val outstandingMessages = new ArrayBlockingQueue[ChatMessage](MagicConstants.MessageBufferLength)
  val recipients = mutable.Set[Recipient]()

  def addRecipient(recipient: Recipient): Unit = {
    recipients.synchronized {
      logger.info(s"Starting to broadcast to $recipient")
      recipients.add(recipient)
    }
  }

  def removeRecipient(recipient: Recipient): Unit = {
    recipients.synchronized {
      logger.info(s"No longer broadcasting to $recipient")
      recipients.remove(recipient)
    }
  }

  def broadcast(message: ChatMessage): Unit = {
    outstandingMessages.put(message)
  }

  override def run(): Unit = {
    Utils.forever {
      val message = outstandingMessages.take()
      recipients.synchronized {
        val toDelete = mutable.ListBuffer[Recipient]()
        recipients foreach { slave =>
          try {
            slave.writeMessage(message)
          } catch {
            case e: IOException =>
              toDelete += slave
          }
        }
        toDelete.foreach(removeRecipient)
      }
    }
  }
}

class SlaveMessageProducer(slave: Slave, broadcaster: SlaveMessageBroadcaster) extends Runnable with Logging {
  override def run(): Unit = {
    broadcaster.addRecipient(slave)
    broadcastMessages()
  }

  @tailrec
  private def broadcastMessages(): Unit = {
    val msg = try {
      slave.readMessage()
    } catch {
      case e: IOException => null
    }
    logger.debug(s"Got message $msg from slave")
    if (msg ne null) {
      msg match {
        case m: ChatMessage => broadcaster.broadcast(m)
        case other => logger.warn(s"Unknown message type $msg")
      }
      broadcastMessages()
    }
  }
}

class Blinker(gpioPort: Int) extends Recipient with Runnable with Logging {
  val blinkCounter = new AtomicInteger()
  val gpioDevicePath = Paths.get(s"/sys/class/gpio/gpio$gpioPort/value")
  val commandOn = "1".getBytes(StandardCharsets.UTF_8)
  val commandOff = "0".getBytes(StandardCharsets.UTF_8)
  Try(Files.write(Paths.get("/sys/class/gpio/export"), gpioPort.toString.getBytes(StandardCharsets.UTF_8)))
  Try(Files.write(Paths.get(s"/sys/class/gpio$gpioPort/direction"), "out".getBytes(StandardCharsets.UTF_8)))

  override def writeMessage(message: ProtocolMessage): Unit = message match {
    case cm: ChatMessage =>
      logger.debug("Got chat message, blinking")
      blink()
  }

  def blink(): Unit = blinkCounter.incrementAndGet()

  override def run(): Unit = {
    while (true) {
      while (blinkCounter.get() > 0) {
        logger.debug("Blink counter > 0, starting to blink")
        blinkCounter.decrementAndGet()
        1 to 4 foreach { _ =>
          turnLEDOn()
          Thread.sleep(300)
          turnLEDOff()
          Thread.sleep(100)
        }
        logger.debug("Done blinking")
      }
    }
  }


  def turnLEDOff(): Unit = {
    Try(Files.write(gpioDevicePath, commandOff))
  }

  def turnLEDOn(): Unit = {
    Try(Files.write(gpioDevicePath, commandOn))
  }
}

object Master extends App with Logging {
  val opts = CommandLineArgs.parse(args)
  val extraRecipients = opts.led.map(l => {
    val blinker = new Blinker(l)
    new Thread(blinker).start()
    blinker
  }).toSeq
  val serverSocket = new ServerSocket(opts.port)
  logger.info(s"Starting to listen on ${serverSocket.getLocalPort}")
  val server = new MasterListener(serverSocket, extraRecipients = extraRecipients)
  server.run()
}

object UITest extends App {
  val broadcaster = new SlaveMessageBroadcaster
  new MasterUI(broadcaster)
}

class MasterUI(broadcaster: SlaveMessageBroadcaster) extends Recipient {
  val frame = new JFrame("Master Frame")

  val gridbag = new GridBagLayout
  frame.setLayout(gridbag)
  val listModel = new DefaultListModel[String]
  val list = new JList(listModel)
  val listBox = new JScrollPane(list)
  val input = new JTextField(10)
  val sendButton = new JButton("send")

  val constraints = new GridBagConstraints()

  def scrollToBottom(): Unit = {
    SwingUtilities.invokeLater(new Runnable {
      override def run(): Unit = {
        val scrollBar = listBox.getVerticalScrollBar
        scrollBar.setValue(scrollBar.getMaximum)
      }
    })
  }

  val sendMessage = new ActionListener() {
    override def actionPerformed(e: ActionEvent): Unit = {
      val text = input.getText
      if (text.exists(!_.isWhitespace)) {
        broadcaster.broadcast(ChatMessage("master", text))
        input.setText("")
      }
      scrollToBottom()
    }
  }

  constraints.fill = GridBagConstraints.BOTH
  constraints.gridwidth = GridBagConstraints.REMAINDER
  gridbag.setConstraints(listBox, constraints)
  frame.add(listBox)

  constraints.gridwidth = 1
  gridbag.setConstraints(input, constraints)
  frame.add(input)
  input.addActionListener(sendMessage)

  gridbag.setConstraints(sendButton, constraints)
  frame.add(sendButton)
  sendButton.addActionListener(sendMessage)

  frame.pack()

  frame.addWindowListener(new WindowAdapter {
    override def windowClosing(e: WindowEvent): Unit = System.exit(0)
  })

  def start(): Unit = {
    frame.setVisible(true)
  }

  override def writeMessage(message: ProtocolMessage): Unit = {
    message match {
      case ChatMessage(sender, content) =>
        listBox.revalidate()
        val scrollBar = listBox.getVerticalScrollBar
        val wasScrolledToMax = scrollBar.getValue + scrollBar.getVisibleAmount == scrollBar.getMaximum
        listModel.addElement(content)
        if (wasScrolledToMax) {
          scrollToBottom()
        }
    }
  }

  override def toString: String = "Master UI"
}

class ArgParseFailException extends RuntimeException("Parsing command line args failed")

case class CommandLineArgs(port: Int = 0, led: Option[Int] = None)

object CommandLineArgs {
  val maxPort = (1 << 16) - 1

  def parse(args: Seq[String]): CommandLineArgs = {
    val parser = new scopt.OptionParser[CommandLineArgs]("master") {
      head("master", "1.0-alpha")
      opt[Int]('p', "port") action { (port, args) =>
        args.copy(port = port)
      } validate { port =>
        if (port < 0 || port > maxPort) {
          failure(s"Port must be between 0 and $maxPort")
        } else {
          success
        }
      } text s"The port the master should listen on, between 0 and $maxPort"
      opt[Int]('l', "led") action { (led, args) =>
        args.copy(led = Some(led))
      } text "(optional) The LED port to make blink upon receiving a message"
    }
    parser.parse(args, CommandLineArgs()) getOrElse (throw new ArgParseFailException)
  }
}