package protocol

import java.io._
import java.lang.reflect.Type
import java.nio.charset.StandardCharsets

import com.google.gson._
import com.google.gson.stream.{JsonReader, JsonWriter}


sealed trait ProtocolMessage
case class ChatMessage(sender: String, messageContent: String) extends ProtocolMessage

case class ProtocolMessageWrapper(messageType: String, messageBody: ProtocolMessage)

class MessageWrapperDeserializer extends JsonDeserializer[ProtocolMessageWrapper] {
  override def deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): ProtocolMessageWrapper = {
    val obj = json.getAsJsonObject
    val messageType = obj.get("messageType").getAsString
    ProtocolMessageWrapper(
      messageType = messageType,
      messageBody = context.deserialize(obj.get("messageBody"), ProtocolMessage.messageTypeNameToClass(messageType))
    )
  }
}

object ProtocolMessage {
  val gsonBuilder = new GsonBuilder
  gsonBuilder.registerTypeAdapter(classOf[ProtocolMessageWrapper], new MessageWrapperDeserializer)
  val gson = gsonBuilder.create()

  def readFromStream(in: InputStream): ProtocolMessage = {
    val wrapper = gson.fromJson[ProtocolMessageWrapper](new JsonReader(new InputStreamReader(in)), classOf[ProtocolMessageWrapper])
    if(wrapper ne null) {
      wrapper.messageBody
    } else {
      null
    }
  }

  def writeToStream(out: OutputStream, message: ProtocolMessage): Unit = {
    val wrapper = ProtocolMessageWrapper(messageClassToTypeName(message), message)
    val writer: JsonWriter = new JsonWriter(new OutputStreamWriter(out))
    gson.toJson(wrapper, classOf[ProtocolMessageWrapper], writer)
    writer.flush()
  }

  def messageTypeNameToClass(typeName: String): Class[_] = {
    typeName match {
      case "chatMessage" => classOf[ChatMessage]
      case _ => throw new RuntimeException(s"Unknown type name: $typeName")
    }
  }

  def messageClassToTypeName(message: ProtocolMessage): String = {
    message match {
      case _: ChatMessage => "chatMessage"
      case _ => throw new RuntimeException(s"No type name defined for ${message.getClass}")
    }
  }

}

object ProtocolTest extends App {
  val out = new ByteArrayOutputStream()
  val message = ChatMessage("master", "Hello world from master")
  ProtocolMessage.writeToStream(out, message)
  val outString = new String(out.toByteArray, StandardCharsets.UTF_8)
  println(s"Output $outString")
  val in = new ByteArrayInputStream(outString.getBytes(StandardCharsets.UTF_8))
  val readMessage = ProtocolMessage.readFromStream(in)
  println(readMessage)
  println(message == readMessage)
}